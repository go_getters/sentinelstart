#!/bin/bash
wget https://github.com/mongodb/mongo-c-driver/releases/download/1.16.2/mongo-c-driver-1.16.2.tar.gz
tar xzf mongo-c-driver-1.16.2.tar.gz && cd mongo-c-driver-1.16.2/
mkdir cmake-build && cd cmake-build
cmake -DENABLE_AUTOMATIC_INIT_AND_CLEANUP=OFF ..
make
sudo make install
cd ../..
curl -OL https://github.com/mongodb/mongo-cxx-driver/archive/r3.4.2.tar.gz
tar -xzf r3.4.2.tar.gz && cd mongo-cxx-driver-r3.4.2/
cd build &&  cmake .. -DCMAKE_BUILD_TYPE=Release  -DCMAKE_INSTALL_PREFIX=/usr/local
sudo cmake --build . --target EP_mnmlstc_core
make && sudo make install
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local:/usr/share/pkgconfig
rm -rf r3.4.2.tar.gz mongo-cxx-driver-r3.4.2/ mongo-c-driver-1.16.2 mongo-c-driver-1.16.2.tar.gz 
sudo ldconfig