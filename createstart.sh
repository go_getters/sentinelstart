#!/bin/bash

FILE1="$HOME/rosstart.service"

cat <<END > $FILE1

[Unit]
Description=rosstart service

[Service]
Type=simple
Restart=on-failure
RestartSec=5s
User=$USER

ExecStart=/bin/bash $HOME/roslaunch.sh

[Install]
WantedBy=multi-user.target

END
chmod a+x $FILE1
echo "running ...."
$FILE1

sudo mv $FILE1 /etc/systemd/system/rosstart.service
echo "rosstart done"

################################################

FILE2="$HOME/roslaunch.sh"

cat <<END > $FILE2

#!/bin/bash
sleep 20
source $HOME/modmanipros2/install/setup.bash
(cd $HOME/modmanipros2/ ; ros2 run modmanipros2 compose --ros-args --params-file $HOME/modmanipros2/config/modmanipconfig.yaml 1>roslog.txt 2>roserr.txt)


END
chmod a+x $FILE2
echo "roslaunch done"

################################################

FILE3="$HOME/permission.service"

cat <<END > $FILE3

[Unit]
Description=permissions service

[Service]
Type=simple
#Restart=on-failure
#RestartSec=5s
User=root

ExecStart=sh -c "$HOME/permit.sh"

[Install]
WantedBy=multi-user.target

END
chmod a+x $FILE3
echo "running ...."
$FILE3
sudo mv $FILE3 /etc/systemd/system/permission.service
echo "permission service done"

###########################################
FILE4="$HOME/permit.sh"

cat <<END > $FILE4

#!/bin/bash
sleep 10
chown $USER /dev/ttyS0
chown $USER /dev/gpiomem
chmod a+rw /dev/gpiomem


END
chmod a+x $FILE4
echo "running ...."
$FILE4
echo "permission script done"




